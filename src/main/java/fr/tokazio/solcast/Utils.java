package fr.tokazio.solcast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static final SimpleDateFormat DATE_FMT = new SimpleDateFormat("dd-MM-yyyy");

    private Utils() {
        super();
    }

    public static Date asDate(final String dateStr) {
        try {
            return DATE_FMT.parse(dateStr);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
