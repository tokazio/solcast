package fr.tokazio.solcast;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.List;

@JsonDeserialize(using = SolcastDataListDeserializer.class)
public class SolcastDataList {

    private final List<SolcastData> list = new ArrayList<>();

    public List<SolcastData> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "SolcastDataList{" +
                "list=" + list +
                '}';
    }

    public void add(final SolcastData solcastData) {
        list.add(solcastData);
    }
}
