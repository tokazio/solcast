package fr.tokazio.solcast;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Date;
import java.util.Iterator;

public class SolcastDataListDeserializer extends StdDeserializer<SolcastDataList> {

    private static final BigDecimal MILLE = new BigDecimal(1000);

    public SolcastDataListDeserializer() {
        this(null);
    }

    public SolcastDataListDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public SolcastDataList deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
        final JsonNode node = jp.getCodec().readTree(jp);
        final SolcastDataList dl = new SolcastDataList();
        final ArrayNode arrayNode = (ArrayNode) node.get("estimated_actuals");
        final Iterator<JsonNode> itr = arrayNode.elements();
        while (itr.hasNext()) {
            final JsonNode n = itr.next();
            final String pvEstimate = n.get("pv_estimate").asText();
            final String periodEnd = n.get("period_end").asText();
            dl.add(new SolcastData(
                    new BigDecimal(pvEstimate).multiply(MILLE).setScale(1, RoundingMode.HALF_EVEN),
                    Date.from(Instant.parse(periodEnd))));
        }
        return dl;
    }
}
