package fr.tokazio.solcast;

import io.quarkus.cache.CacheResult;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author Romain PETIT <tokazio@esyo.net>
 */
@Path("/solcast")
@Produces(MediaType.APPLICATION_JSON)
public class SolcastResource {

    @Inject
    SolcastRepository repository;

    @GET
    @CacheResult(cacheName = "solcast-cache")
    public List<SolcastEntity> all(@QueryParam("from") String from, @QueryParam("to") String to) {
        return repository.all(from, to);
    }

}
