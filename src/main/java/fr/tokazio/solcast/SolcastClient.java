package fr.tokazio.solcast;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;

@Path("/rooftop_sites")
@RegisterRestClient(configKey = "solcast-client")
@Consumes("application/json")
@Produces("application/json")
public interface SolcastClient {

    @GET
    @Path("{resource_id}/estimated_actuals")
    SolcastDataList getData(@PathParam("resource_id") String resourceId, @QueryParam("hours") int hours, @QueryParam("format") String format, @QueryParam("api_key") String apiKey);

}
