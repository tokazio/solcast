package fr.tokazio.solcast.zeroconf;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.scheduler.Scheduled;
import org.captainunlikely.zeroconf.Service;
import org.captainunlikely.zeroconf.Zeroconf;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

@ApplicationScoped
public class RegisteringZeroConfService {

    public static final String ERROR_PUBLISHING_ZEROCONF = "Erreur publishing zeroconf";

    @ConfigProperty(name = "quarkus.http.port")
    int port;

    @ConfigProperty(name = "bonjour.alias")
    String bonjourAlias;

    @ConfigProperty(name = "bonjour.service")
    String bonjourService;

    @Inject
    Logger log;

    Zeroconf zeroconf = new Zeroconf();
    Service service;

    @PostConstruct
    void init() {
        try {
            zeroconf.addAllNetworkInterfaces();
            service = zeroconf.newService(bonjourAlias, bonjourService, port).putText("ips", allIps());
        } catch (Exception ex) {
            log.warn(ERROR_PUBLISHING_ZEROCONF, ex);
        }
    }

    @Scheduled(every = "30m")
    void announce() {
        try {
            service.announce();
            log.info("Declared bonjour " + service.getInstanceName() + " on ips: " + allIps() + " and port " + port);
        } catch (Exception ex) {
            log.warn(ERROR_PUBLISHING_ZEROCONF, ex);
        }
    }

    void unregister(@Observes ShutdownEvent ev) {
        log.info("Application stopping");
        cancel();
    }

    void cancel() {
        try {
            service.cancel();
            log.info("UNdeclared zeroconf " + service.getInstanceName() + " on ips: " + allIps() + " and port " + port);
        } catch (Exception ex) {
            log.warn(ERROR_PUBLISHING_ZEROCONF, ex);
        }
    }

    String allIps() throws SocketException {
        final StringBuilder ips = new StringBuilder();
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface iface = interfaces.nextElement();
            // filters out 127.0.0.1 and inactive interfaces
            if (iface.isLoopback() || !iface.isUp())
                continue;

            Enumeration<InetAddress> addresses = iface.getInetAddresses();
            while (addresses.hasMoreElements()) {
                InetAddress addr = addresses.nextElement();
                //v4 only
                if (addr.getHostAddress().contains(".")) {
                    ips.append(iface.getDisplayName()).append("=").append(addr.getHostAddress()).append(",");
                }
            }
        }
        return ips.toString();
    }
}
