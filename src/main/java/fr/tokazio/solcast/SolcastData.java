package fr.tokazio.solcast;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SolcastData {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @JsonProperty("pv_estimate")
    private final BigDecimal pwEstimate;
    @JsonProperty("period_end")
    private final Date date;

    public SolcastData(final BigDecimal pwEstimate, final Date date) {
        this.pwEstimate = pwEstimate;
        this.date = date;
    }

    public BigDecimal getPwEstimate() {
        return pwEstimate;
    }

    public Date getDate() {
        return date;
    }

    public String getDateAsString() {
        return DATE_FORMAT.format(this.date);
    }

    @Override
    public String toString() {
        return "SolcastData{" +
                "pwEstimate='" + pwEstimate + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
