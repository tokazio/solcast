package fr.tokazio.solcast;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Entity
@Cacheable
public class SolcastEntity {

    @Id
    private final String id = UUID.randomUUID().toString();
    private BigDecimal pwEstimate;
    private Date date;

    SolcastEntity() {
        super();
    }

    public SolcastEntity(final BigDecimal pvEstimate, final Date periodEnd) {
        this.pwEstimate = pvEstimate;
        this.date = periodEnd;
    }

    public String getId() {
        return id;
    }

    public BigDecimal getPwEstimate() {
        return pwEstimate;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "SolcastEntity{" +
                "id='" + id + '\'' +
                ", pvEstimate='" + pwEstimate + '\'' +
                ", periodEnd='" + date + '\'' +
                '}';
    }
}
