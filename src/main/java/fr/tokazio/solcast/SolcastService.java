package fr.tokazio.solcast;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class SolcastService {

    @ConfigProperty(name = "resourceId")
    String resourceId;

    @ConfigProperty(name = "hours")
    int hours;

    @ConfigProperty(name = "apikey")
    String apikey;

    @Inject
    @RestClient
    SolcastClient client;

    public SolcastDataList getFromAPI() {
        return client.getData(resourceId, hours, "json", apikey);
    }

}
