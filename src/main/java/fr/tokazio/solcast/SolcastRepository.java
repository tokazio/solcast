package fr.tokazio.solcast;

import java.util.List;

public interface SolcastRepository {

    List<SolcastEntity> all(String from, String to);

    void save(SolcastDataList data);
}
