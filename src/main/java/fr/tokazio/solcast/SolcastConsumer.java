package fr.tokazio.solcast;

import io.quarkus.scheduler.Scheduled;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;

@ApplicationScoped
public class SolcastConsumer {

    @Inject
    Logger log;

    @Inject
    SolcastService service;

    @Inject
    SolcastRepository repository;

    @Scheduled(every = "{every-solcast}") //! 50 requests max per day
    public void consume() {
        final Date now = new Date();
        if (now.after(start()) && now.before(end())) {
            SolcastDataList data = service.getFromAPI();
            log.info("Got " + data);
            repository.save(data);
        }
    }

    private Date end() {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private Date start() {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 6);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
