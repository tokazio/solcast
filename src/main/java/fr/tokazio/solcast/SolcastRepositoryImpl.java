package fr.tokazio.solcast;

import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@ApplicationScoped
public class SolcastRepositoryImpl implements SolcastRepository {

    @Inject
    Logger log;

    @Inject
    EntityManager em;

    @Override
    public List<SolcastEntity> all(final String from, final String to) {
        if (from == null && to == null) {
            return em.createQuery("from SolcastEntity order by date desc", SolcastEntity.class)
                    .getResultList();
        } else if (from != null && to == null) {
            return em.createQuery("from SolcastEntity where date >= :from order by date desc", SolcastEntity.class)
                    .setParameter("from", Utils.asDate(from))
                    .getResultList();
        } else if (from == null) {
            return em.createQuery("from SolcastEntity where date <= :to order by date desc", SolcastEntity.class)
                    .setParameter("to", Utils.asDate(to))
                    .getResultList();
        }
        return em.createQuery("from SolcastEntity where date >= :from and date <= :to order by date desc", SolcastEntity.class)
                .setParameter("from", Utils.asDate(from))
                .setParameter("to", Utils.asDate(to))
                .getResultList();
    }

    @Transactional
    @Override
    public void save(final SolcastDataList data) {
        //TODO batch
        final Date now = Date.from(Instant.now());
        for (SolcastData d : data.getList()) {
            final SolcastEntity ett = new SolcastEntity(
                    d.getPwEstimate(),
                    d.getDate()
            );
            if (ett.getDate().before(now)) {
                if (!exists(ett)) {
                    em.persist(ett);
                    log.info("Saved: " + ett);
                } else {
                    log.warn("Already exists: " + ett);
                }
            }
        }
    }

    private boolean exists(final SolcastEntity ett) {
        try {
            final SolcastEntity ex = em.createQuery("from SolcastEntity where date=:date", SolcastEntity.class)
                    .setParameter("date", ett.getDate())
                    .getSingleResult();
            return ex != null;
        } catch (NoResultException ex) {
            return false;
        }
    }
}
