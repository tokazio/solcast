# Solcast

Prends les données de Solcast et les mets dans une base H2

Declares itself as a zeroconf/bonjour service at application start.

Putting info in the database every 25 minutes (exepted between 06:00 and 23:00) (see application.properties to change
it)

You must provide your Solcast key and resouce location id in the application.properties

## Service compteur

```
sudo mkdir /opt/solcast
```

```
cd /home/odroid/solcast
```

```
gradle build -Dquarkus.package.type=uber-jar -x test
```

```
sudo cp /home/odroid/solcast/build/solcast-1.0-SNAPSHOT-runner.jar /opt/solcast/solcast.jar
```

sudo nano /etc/systemd/system/solcast.service

```
[Unit]
Description=Solcast Java service

[Service]
WorkingDirectory=/opt/solcast
ExecStart=/usr/bin/java -Xmx128m -server -jar solcast.jar >> /opt/solcast/solcast.log
User=root
Type=simple
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
```

```
sudo systemctl daemon-reload
```

```
sudo systemctl enable solcast
```

```
sudo mkdir /opt/solcast/config
```

```
sudo nano /opt/solcast/config/application.properties
```

Put your apikey, resourceID and hours in this configuration file

## Changing configuration

create a folder /opt/solcast/config with an application.properties file in it to override the embedded one.

```
nano src/main/resources/application.properties
gradle build -Dquarkus.package.type=uber-jar -x test
java -jar build/solcast-1.0-SNAPSHOT-runner.jar
```

## faire le odroid resize.sh

```
wget -O /usr/local/bin/odroid-utility.sh https://raw.githubusercontent.com/mdrjr/odroid-utility/master/odroid-utility.sh
chmod +x /usr/local/bin/odroid-utility.sh /usr/local/bin/odroid-utility.sh
```

## Packaging and running the application

The application can be packaged using:

```shell script
./gradlew build
```

It produces the `quarkus-run.jar` file in the `build/quarkus-app/` directory. Be aware that it’s not an _über-jar_ as
the dependencies are copied into the `build/quarkus-app/lib/` directory.

The application is now runnable using `java -jar build/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:

```shell script
./gradlew build -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar build/*-runner.jar`.
